package com.captton.empresa;

import java.util.ArrayList;

public class Subordinado extends Empleado {
	
	private ArrayList<String> serviciosPagos;

	public Subordinado(String nombre, int numempleado) {
		super(nombre, numempleado);
		serviciosPagos = new ArrayList<String>();
		// TODO Auto-generated constructor stub
	}

	public Subordinado() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String Trabajar() {
		// TODO Auto-generated method stub
		return "Yo los lunes trabajo desde casa";
	}
	
	public void agregarServicio(String serv) {
		
		this.serviciosPagos.add(serv);
	}
	
	public ArrayList<String> getServiciosPagos() {
		
		return this.serviciosPagos;
	}

}
