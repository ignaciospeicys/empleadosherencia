package com.captton.empresa;

public abstract class Empleado {
	
	private String nombre;
	private int numempleado;
	
	public Empleado(String nombre, int numempleado) {
		super();
		this.nombre = nombre;
		this.numempleado = numempleado;
	}

	public Empleado() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumempleado() {
		return numempleado;
	}

	public void setNumempleado(int numempleado) {
		this.numempleado = numempleado;
	}
	
	public String almorzar() {
		
		return "�am �am �am";
	}
	
	public abstract String Trabajar();
	
	public final String trabajarHorasExtras() {
		
		return "Tengo sue�o, dejenme ir";
	}
	
	
	

}
