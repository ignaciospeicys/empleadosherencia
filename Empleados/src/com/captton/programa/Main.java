package com.captton.programa;

import com.captton.empresa.Gerente;
import com.captton.empresa.Subordinado;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Gerente g1 = new Gerente("Ignacio",111);
		System.out.println("Gerente, su nombre es: "+g1.getNombre());
		System.out.println(g1.almorzar());
		System.out.println(g1.Trabajar());
		System.out.println(g1.trabajarHorasExtras());
		
		Subordinado s1 = new Subordinado("Pablo",120);
		s1.agregarServicio("Banda Ancha Internet");
		s1.agregarServicio("Gimnasio");
		
		System.out.println("\nSubordinado, Nombre "+s1.getNombre());
		System.out.println(s1.almorzar());
		System.out.println(s1.Trabajar());
		System.out.println(s1.trabajarHorasExtras());
		
		System.out.println("\nEstos son los servicios pagos");
		
		for (String ser : s1.getServiciosPagos()) {
			
			System.out.println(ser);
		}
		
	}

}
